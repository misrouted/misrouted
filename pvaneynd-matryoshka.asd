;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :asdf)

(defsystem :pvaneynd-matryoshka
  :name "pvanend-matryoshka"
  :author "Peter Van Eynde"
  :version "1"
  :maintainer "Peter Van Eynde"
  :licence "Lisp Lesser General Public License (LLGPL)"
  :description "matryoshka system of items and containers"
  :depends-on ( :fiveam
		:pvaneynd-ip)
  :components
  ((:module
    "matryoshka"
    :components ((:file "package")
		 (:file "namespace" :depends-on ("package"))
		 (:file "item" :depends-on ("namespace"))
		 (:file "container" :depends-on ("item"))
		 (:file "operations" :depends-on ("container"))
		 (:file "tests" :depends-on ("operations"))))))

