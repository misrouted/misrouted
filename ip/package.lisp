;;; -*- MODE: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :common-lisp-user)

(defpackage #:pvaneynd.ip
  (:use :common-lisp)
  (:export #:ipaddress
	   #:ipaddress-ip-as-unsigned-byte
	   #:ipaddress-ip-as-parts

	   #:ipaddress-mask-as-length
	   #:ipaddress-mask-as-parts
	   #:ipaddress-mask-as-unsigned-byte

	   #:ipaddress-cisco-mask-as-parts
	   #:ipaddress-cisco-mask-as-unsigned-byte

	   #:ipaddress-to-string

	   #:parse-ipv4
	   #:parse-ipv6
	   #:parse-ip
	   #:parse-range

	   #:iprange
	   #:iprange-start-range
	   #:iprange-end-range

	   #:ip-contains-p
	   #:ip-has-overlap-p
	   #:ip-in-same-subnet-p
	   ))
