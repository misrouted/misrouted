;;; -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.ip)


(eval-when (:compile-toplevel
	    :load-toplevel
	    :execute)
  (unless (5am:get-test :pvaneynd.ip.suite)
    (5am:def-suite :pvaneynd.ip.suite
	:description "Test ip functions")))

(5am:in-suite :pvaneynd.ip.suite)

;; first an ip
(5am:test :pvaneynd.ip.ip-creation
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (equalp
    (list (ipaddress-ip-as-unsigned-byte ip)
	  (ipaddress-ip-as-parts ip))
    (list #x01020304
	  (list 1 2 3 4)))))
  ;; then a network
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304
	    (list 1 2 3 4)))))
  ;; also via octects:
  ;; ip
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-parts ip)
	   (list 1 2 3 4))
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304
	    (list 1 2 3 4)))))
  ;; network
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-parts ip)
	   (list 1 2 3 4))
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304
	    (list 1 2 3 4))))))
 
;; then a network mask
(5am:test :pvaneynd.ip.network-creation
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-length ip)
	   0)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #x00000000
	    0
	    (list 0 0 0 0)
	    #xFFFFFFFF
	    (list 255 255 255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-length ip)
	   32)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFFFFFFF
	    32
	    (list 255 255 255 255)
	    0
	    (list 0 0 0 0)
	    ))))
  
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-unsigned-byte ip)
	   #xFFF00000)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-length ip)
	   12)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-parts ip)
	   (list 255 240 0 0))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	   #x000FFFFF)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-network-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-cisco-mask-as-parts ip)
	   (list 0   15  255 255))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    )))))

;; test promotion of objects
(5am:test :pvaneynd.ip.ip-promotion
  ;; and now all the same with automatic promotion:
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-unsigned-byte ip)
	   #xFFF00000)
     (typep ip
	    'ipnetwork-ipv4)))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-unsigned-byte ip)
	   #xFFF00000)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-length ip)
	   12)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-mask-as-parts ip)
	   (list 255 240 0 0))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	   #x000FFFFF)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    ))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv4)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304)
     (setf (ipaddress-cisco-mask-as-parts ip)
	   (list 0   15  255 255))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000
	    12
	    (list 255 240 0 0)
	    #x000FFFFF
	    (list 0   15  255 255)
	    )))))

;;; test creation errors
(5am:test :pvaneynd.ip.ip-creation-errors
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-unsigned-byte ip)
	  -1)))
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-unsigned-byte ip)
	  (expt 2 64))))
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-unsigned-byte ip)
	  0.2)))
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-parts ip)
	  1)))
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-parts ip)
	  (list 1 2 3 4 5))))
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-parts ip)
	  (list 1 2 3 800))))
  (5am:signals error
  (let ((ip (make-ipaddress-ipv4)))
    (setf (ipaddress-ip-as-parts ip)
	  (list 1 2 3 -1)))))

(5am:test :pvaneynd.ip.ip-mask-creation-errors
  ;; now masks
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-length ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-length ip)
	    33)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    (expt 2 64))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    0.2)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    #xFF00FF00)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-parts ip)
	    1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-parts ip)
	    (list 255 255 255 255 255))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-parts ip)
	    (list 255 255 255 800))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-parts ip)
	    (list 255 255 255 -1))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-parts ip)
	    (list 255 0 255 255))))
  ;;; cisco masks
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (expt 2 64))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    0.2)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    #xFF00FF00)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list 255 255 255 255 255))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list 255 255 255 800))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list 255 255 255 -1))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list 255 0 255 255)))))

(5am:test :pvaneynd.ip.ipv4-parse
  (5am:is-true
   (let ((ip (parse-ipv4 "9.36.75.113")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list 9 36 75 113))))
  (5am:is-true
   (let ((ip (parse-ipv4 "0x09244B71")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list 9 36 75 113))))
  (5am:is-true
   (let ((ip (parse-ipv4 "9.36.75.113/24")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "9.36.75.113/255.255.255.0")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "9.36.75.113/0xFFFFFF00")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))

  (5am:is-true
   (let ((ip (parse-ipv4 "0x09244B71/24")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "0x09244B71/255.255.255.0")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "0x09244B71/0xFFFFFF00")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))

  (5am:is-true
   (let ((ip (parse-ipv4 "9.36.75.113/0.0.0.255")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "9.36.75.113/0x000000FF")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "0x09244B71/0.0.0.255")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24))))
  (5am:is-true
   (let ((ip (parse-ipv4 "0x09244B71/0x000000FF")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list 
       (list 9 36 75 113)
       24)))))

(5am:test :pvaneynd.ip.ip-range
  (5am:is-true
   (let ((range (make-range-ipv4)))
     (setf (iprange-start-range range)
	   (parse-ipv4 "9.36.75.113"))
     (setf (iprange-end-range range)
	   (parse-ipv4 "9.36.75.213"))
     (equalp
      (list
       (ipaddress-ip-as-parts
	(iprange-start-range range))
       (ipaddress-ip-as-parts
	(iprange-end-range range)))
      (list
       (list 9 36 75 113)
       (list 9 36 75 213)))))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-start-range
      (parse-ipv4
       "9.36.75.113")))
    (list
     9 36 75 113)))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-end-range
      (parse-ipv4
       "9.36.75.113")))
    (list
     9 36 75 113)))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-start-range
      (parse-ipv4
       "9.36.75.113/24")))
    (list
     9 36 75 0)))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-end-range
      (parse-ipv4
       "9.36.75.113/24")))
    (list
     9 36 75 255)))
  (5am:is-true
   (let ((range (make-range-ipv4)))
     (setf (iprange-start-range range)
	   (parse-ipv4 "9.36.75.113"))
     (setf (iprange-end-range range)
	   (parse-ipv4 "9.36.75.213"))
     (equalp
      (ipaddress-ip-as-parts
	range)
       (list 9 36 75 113)))))


;;; ipv6

(5am:test :pvaneynd.ip.ipv6.ip-creation
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304050607080910111213141516
	    (list #x0102 #x0304 #x0506 #x0708
		  #x0910 #x1112 #x1314 #x1516)))))
  ;; then a network
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304050607080910111213141516
	    (list #x0102 #x0304 #x0506 #x0708
		  #x0910 #x1112 #x1314 #x1516)))))
  ;; also via octects:
  ;; ip
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-parts ip)
	   (list #x0102 #x0304 #x0506 #x0708
		 #x0910 #x1112 #x1314 #x1516))
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304050607080910111213141516
	    (list #x0102 #x0304 #x0506 #x0708
		  #x0910 #x1112 #x1314 #x1516)))))
  ;; network
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-parts ip)
	   (list #x0102 #x0304 #x0506 #x0708
		 #x0910 #x1112 #x1314 #x1516))
     (equalp
      (list (ipaddress-ip-as-unsigned-byte ip)
	    (ipaddress-ip-as-parts ip))
      (list #x01020304050607080910111213141516
	    (list #x0102 #x0304 #x0506 #x0708
		  #x0910 #x1112 #x1314 #x1516))))))
 
;; then a network mask
(5am:test :pvaneynd.ip.ipv6.network-creation
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-length ip)
	   0)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #x00000000000000000000000000000000
	    0
	    (list #x0000 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #xFFFF #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))	   
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-length ip)
	   128)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))


      (list #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    128
	    (list #xFFFF #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)
	    #x00000000000000000000000000000000
	    (list #x0000 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)))))
  
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-unsigned-byte ip)
	   #xFFF00000000000000000000000000000)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))

      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-length ip)
	   12)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-parts ip)
	   (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	   #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-network-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-cisco-mask-as-parts ip)
	   (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))
      
      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF))))))

;; test promotion of objects
(5am:test :pvaneynd.ip.ipv6.ip-promotion
  ;; and now all the same with automatic promotion:
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-unsigned-byte ip)
	   #xFFF00000000000000000000000000000)
     (typep ip
	    'ipnetwork-ipv6)))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-unsigned-byte ip)
	   #xFFF00000000000000000000000000000)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))

      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-length ip)
	   12)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))

      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-mask-as-parts ip)
	   (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))

      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	   #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))

      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))
  (5am:is-true
   (let ((ip (make-ipaddress-ipv6)))
     (setf (ipaddress-ip-as-unsigned-byte ip)
	   #x01020304050607080910111213141516)
     (setf (ipaddress-cisco-mask-as-parts ip)
	   (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF))
     (equalp
      (list (ipaddress-mask-as-unsigned-byte ip)
	    (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-parts ip)
	    (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (ipaddress-cisco-mask-as-parts ip))

      (list #xFFF00000000000000000000000000000
	    12
	    (list #xFFF0 #x0000 #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000)
	    #x000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
	    (list #x000F #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF))))))

;;; test creation errors
(5am:test :pvaneynd.ip.ipv6.ip-creation-errors
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    (expt 2 129))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    0.2)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-parts ip)
	    1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-parts ip)
	    (list 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-parts ip)
	    (list #xFFFF #xFFFFF #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-ip-as-parts ip)
	    (list #xFFFF #xFFFFF #x0000 -1
		  #x0000 #x0000 #x0000 #x0000)))))

(5am:test :pvaneynd.ip.ipv6.ip-mask-creation-errors
  ;; now masks
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-length ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-length ip)
	    330)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    (expt 2 129))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    0.2)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-unsigned-byte ip)
	    #xFFF00000000000000000000000F00000)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-parts ip)
	    1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-parts ip)
	    (list #xFFFF #xFFFF #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000
		  #x0000))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-parts ip)
	    (list #xFFFF #xFFFFF #x0000 #x0000
		  #x0000 #x0000 #x0000 #x0000))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-parts ip)
	    (list #xFFFF #xFFFF -1 #x0000
		  #x0000 #x0000 #x0000 #x0000))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-parts ip)
	    (list #xFFFF #xFFFF #x0000 #x00F0
		  #x0000 #x0000 #x0000 #x0000))))
  ;;; cisco masks
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    -1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    (expt 2 129))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    0.2)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
	    #x100FFFFFFFFFFFFFFFFFFFFFFFFFFFFF)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    1)))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list #xFFFF #xFFFF #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list #xFFFF #xFFFF #xFFFF #xFFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list #xFFFF #xFFFF #xFFFF #xFFFF
		  #xFFFF #xFFFF -1 #xFFFF))))
  (5am:signals error
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-cisco-mask-as-parts ip)
	    (list #xFFFF #xFFFF 0 #xFFFF
		  #xFFFF #xFFFF #xFFFF #xFFFF)))))

(5am:test :pvaneynd.ip.ipv6.ipv6-parse
  (5am:is-true
   (let ((ip (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #xFEDC #xBA98 #x7654 #x3210 #xFEDC #xBA98 #x7654 #x3210))))
  (5am:is-true
   (let ((ip (parse-ipv6 "1080:0:0:0:8:800:200C:417A")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x1080 #x0 #x0 #x0 #x8 #x800 #x200C #x417A))))

  (5am:is-true
   (let ((ip (parse-ipv6 "1080:0:0:0:8:800:200C:417A")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x1080 #x0 #x0 #x0 #x8 #x800 #x200C #x417A))))
  (5am:is-true
   (let ((ip (parse-ipv6 "1080::8:800:200C:417A")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x1080 #x0 #x0 #x0 #x8 #x800 #x200C #x417A))))

  (5am:is-true
   (let ((ip (parse-ipv6 "FF01:0:0:0:0:0:0:101")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #xFF01 #x0 #x0 #x0 #x0 #x0 #x0 #x101))))
  (5am:is-true
   (let ((ip (parse-ipv6 "FF01::101")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #xFF01 #x0 #x0 #x0 #x0 #x0 #x0 #x101))))
  (5am:is-true
   (let ((ip (parse-ipv6 "FF01:101::")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #xFF01 #x101 #x0 #x0 #x0 #x0 #x0 #x0))))
  (5am:is-true
   (let ((ip (parse-ipv6 "::FF01:101")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #xFF01 #x101))))
  
  (5am:is-true
   (let ((ip (parse-ipv6 "0:0:0:0:0:0:0:1")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #x0 #x1))))
  (5am:is-true
   (let ((ip (parse-ipv6 "::1")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #x0 #x1))))
  
  (5am:is-true
   (let ((ip (parse-ipv6 "0:0:0:0:0:0:0:0")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #x0 #x0))))
  (5am:is-true
   (let ((ip (parse-ipv6 "::")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #x0 #x0))))

  (5am:is-true
   (let ((ip (parse-ipv6 "0:0:0:0:0:0:13.1.68.3")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #x0D01 #x4403))))
  (5am:is-true
   (let ((ip (parse-ipv6 "::13.1.68.3")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #x0 #x0D01 #x4403))))

  (5am:is-true
   (let ((ip (parse-ipv6 "1234:0:0:0:0:0:13.1.68.3")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x1234 #x0 #x0 #x0 #x0 #x0 #x0D01 #x4403))))
  (5am:is-true
   (let ((ip (parse-ipv6 "1234::13.1.68.3")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x1234 #x0 #x0 #x0 #x0 #x0 #x0D01 #x4403))))
  
  (5am:is-true
   (let ((ip (parse-ipv6 "0:0:0:0:0:FFFF:129.144.52.38")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #xFFFF #x8190 #x3426))))
  (5am:is-true
   (let ((ip (parse-ipv6 "::FFFF:129.144.52.38")))
     (equalp
      (ipaddress-ip-as-parts ip)
      (list #x0 #x0 #x0 #x0 #x0 #xFFFF #x8190 #x3426))))

  (5am:is-true
   (let ((ip (parse-ipv6 "12AB:0000:0000:CD30:0000:0000:0000:0000/60")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list
       (list #x12AB #x0000 #x0000 #xCD30 #x0000 #x0000 #x0000 #x0000)
       60))))
  (5am:is-true
   (let ((ip (parse-ipv6 "12AB::CD30:0:0:0:0/60")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list
       (list #x12AB #x0000 #x0000 #xCD30 #x0000 #x0000 #x0000 #x0000)
       60))))
  (5am:is-true
   (let ((ip (parse-ipv6 "12AB:0:0:CD30::/60")))
     (equalp
      (list
       (ipaddress-ip-as-parts ip)
       (ipaddress-mask-as-length ip))
      (list
       (list #x12AB #x0000 #x0000 #xCD30 #x0000 #x0000 #x0000 #x0000)
       60)))))

(5am:test :pvaneynd.ip.ipv6.ip-range
  (5am:is-true
   (let ((range (make-range-ipv6)))
     (setf (iprange-start-range range)
	   (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210"))
     (setf (iprange-end-range range)
	   (parse-ipv6 "1080:0:0:0:8:800:200C:417A"))
     (equalp
      (list
       (ipaddress-ip-as-parts
	(iprange-start-range range))
       (ipaddress-ip-as-parts
	(iprange-end-range range)))
      (list
       (list #xFEDC #xBA98 #x7654 #x3210 #xFEDC #xBA98 #x7654 #x3210)
       (list #x1080 #x0 #x0 #x0 #x8 #x800 #x200C #x417A)))))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-start-range
      (parse-ipv6
       "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210")))
    (list #xFEDC #xBA98 #x7654 #x3210 #xFEDC #xBA98 #x7654 #x3210)))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-end-range
      (parse-ipv6
       "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210")))
    (list #xFEDC #xBA98 #x7654 #x3210 #xFEDC #xBA98 #x7654 #x3210)))
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-start-range
      (parse-ipv6
       "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210/64")))
    (list #xFEDC #xBA98 #x7654 #x3210 #x0000 #x0000 #x0000 #x0000)))
   
  (5am:is-true
   (equalp
    (ipaddress-ip-as-parts
     (iprange-end-range
      (parse-ipv6
       "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210/64")))
    (list #xFEDC #xBA98 #x7654 #x3210 #xFFFF #xFFFF #xFFFF #xFFFF)))

  (5am:is-true
   (let ((range (make-range-ipv6)))
     (setf (iprange-start-range range)
	   (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210"))
     (setf (iprange-end-range range)
	   (parse-ipv6 "1080:0:0:0:8:800:200C:417A"))
     (equalp
      (ipaddress-ip-as-parts
       range)
      (list #xFEDC #xBA98 #x7654 #x3210 #xFEDC #xBA98 #x7654 #x3210)))))

(5am:test :pvaneynd.ip.range.parse
  (5am:is-true
   (let ((range (parse-range "9.36.75.113-9.36.75.213")))
     (equalp
      (list
       (ipaddress-ip-as-parts
	(iprange-start-range range))
       (ipaddress-ip-as-parts
	(iprange-end-range range)))
      (list
       (list 9 36 75 113)
       (list 9 36 75 213)))))
  (5am:is-true
   (let ((range (parse-range "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210-1080:0:0:0:8:800:200C:417A")))
     (equalp
      (list
       (ipaddress-ip-as-parts
	(iprange-start-range range))
       (ipaddress-ip-as-parts
	(iprange-end-range range)))
      (list
       (list #xFEDC #xBA98 #x7654 #x3210 #xFEDC #xBA98 #x7654 #x3210)
       (list #x1080 #x0 #x0 #x0 #x8 #x800 #x200C #x417A))))))

(5am:test :pvaneynd.ip.contains-p
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.13"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.75.14"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.75.13"))
	 (ip-b (parse-ipv4 "9.36.75.14")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.76.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.14/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-contains-p ip-a ip-b)))  
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.76.14/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.0/24")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.0/25")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.76.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.1/24"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.128")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-b (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-a (parse-ipv4 "9.36.75.128/16")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-ipv4 "9.36.78.128/25")))
     (ip-contains-p ip-a ip-b)))
 (5am:is-false
   (let ((ip-b (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-a (parse-ipv4 "9.36.78.128/25")))
     (ip-contains-p ip-a ip-b)))
 (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-range "9.36.75.0-9.36.75.254")))
     (ip-contains-p ip-a ip-b)))
 (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-range "9.36.75.10-9.36.75.254")))
     (ip-contains-p ip-a ip-b)))
 (5am:is-false
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-range "9.35.75.0-9.35.75.254")))
     (ip-contains-p ip-a ip-b))))

(5am:test :pvaneynd.ip.has-overlap-p
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.13"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.75.14"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.75.13"))
	 (ip-b (parse-ipv4 "9.36.75.14")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.76.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.14/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-has-overlap-p ip-a ip-b)))  
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.76.14/24"))
	 (ip-b (parse-ipv4 "9.36.75.13")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.0/24")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.0/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv4 "9.36.76.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.1/24"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-ipv4 "9.36.75.128")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-ipv4 "9.36.75.128/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-b (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-a (parse-ipv4 "9.36.75.128/16")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-ipv4 "9.36.78.128/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-b (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-a (parse-ipv4 "9.36.78.128/25")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-range "9.36.75.0-9.36.75.254")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-range "9.36.75.10-9.36.75.254")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-range "9.36.76.0-9.36.76.254"))
	 (ip-b (parse-range "9.36.75.0-9.36.75.254")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-range "9.36.75.0-9.36.76.254"))
	 (ip-b (parse-range "9.36.74.0-9.36.75.2")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-b (parse-ipv4 "9.36.75.0/24"))
	 (ip-a (parse-range "9.36.75.250-9.36.76.254")))
     (ip-has-overlap-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-ipv4 "9.36.75.0/24"))
	 (ip-b (parse-range "9.36.75.250-9.36.76.254")))
     (ip-has-overlap-p ip-a ip-b))))


(5am:test :pvaneynd.ip.contains-p.ipv6
  (5am:is-true
   (let ((ip-a (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210"))
	 (ip-b (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv6 "FEDC:BA99:7654:3210:FEDC:BA98:7654:3210"))
	 (ip-b (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210")))
     (ip-contains-p ip-a ip-b)))

  (5am:is-true
   (let ((ip-a (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210/64"))
	 (ip-b (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:DA98:6654:3310")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false 
   (let ((ip-a (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210/64"))
	 (ip-b (parse-ipv6 "FEDC:BB98:7654:3210:FEDC:DA98:6654:3310")))
     (ip-contains-p ip-a ip-b)))

  (5am:is-true
   (let ((ip-a (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210/64"))
	 (ip-b (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:DA98:6654:3310/120")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-false
   (let ((ip-a (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210/64"))
	 (ip-b (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:DA98:6654:3310/12")))
     (ip-contains-p ip-a ip-b)))
  (5am:is-true
   (let ((ip-a (parse-range "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210-FEDC:BA98:7655:3210:FEDC:BA98:7654:3210"))
	 (ip-b (parse-ipv6 "FEDC:BA98:7654:3210:FEDC:BA98:7654:3211")))
     (ip-contains-p ip-a ip-b)))
  
  (5am:is-false
   (let ((ip-a (parse-range "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210-FEDC:BA98:7655:3210:FEDC:BA98:7654:3210"))
	 (ip-b (parse-ipv6 "FEDE:BA98:7654:3210:FEDC:BA98:7654:3211")))
     (ip-contains-p ip-a ip-b)))

  (5am:is-true
   (let ((ip-a (parse-range "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210-FEDC:BA98:7655:3210:FEDC:BA98:7654:3210"))
	 (ip-b (parse-range "FEDC:BA98:7654:3210:FEDC:BA98:7654:3211-FEDC:BA98:7654:3210:FEDC:BA98:7654:3311")))
     (ip-contains-p ip-a ip-b))))