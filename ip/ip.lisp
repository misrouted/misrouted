;;; -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.ip)


(defclass ipaddress ()
  ()
  (:documentation
   "This is the main ipaddress class, specific ipv4 and ipv6 descendants exist"))

(defclass ipnetwork (ipaddress)
  ()
  (:documentation
   "This is the main ipnetwork class.
It does not only demote a network but also a ip with mask."))

(declaim (ftype (function (ipaddress) unsigned-byte)
		ipaddress-ip-as-unsigned-byte
		ipaddress-mask-as-unsigned-byte
		ipaddress-cisco-mask-as-unsigned-byte)
	 (ftype (function (ipaddress) integer)
		ipaddress-mask-as-length
		ipaddress-cisco-mask-as-length))

(defclass ipaddress-ipv4 (ipaddress)
  ((ip-value :type (unsigned-byte 32)
	     :initform 0
	     :initarg :ip-value
	     :documentation
	     "The ip itself as a unsigned-byte")))

(defclass ipaddress-ipv6 (ipaddress)
  ((ip-value :type (unsigned-byte 128)
	     :initform 0
	     :initarg :ip-value
	     :documentation
	     "The ip itself as a unsigned-byte")))

(defclass ipnetwork-ipv4 (ipaddress-ipv4 ipnetwork)
  ((mask-length :type (integer 0 32)
		:initform 0
		:initarg :mask-length
		:documentation
		"The length of the network mask")))

(defclass ipnetwork-ipv6 (ipaddress-ipv6 ipnetwork)
  ((mask-length :type (integer 0 128)
		:initform 0
		:initarg :mask-length
		:documentation
		"The length of the network mask")))

(defclass iprange ()
  ((end-range   :type ipaddress
		:initarg :end))
  (:documentation
   "This class represents an ip address range. The start address is the inherited 'address' of
the class"))

(defclass iprange-ipv4 (iprange ipaddress-ipv4)
  ())

(defclass iprange-ipv6 (iprange ipaddress-ipv6)
  ())

(defun make-ipaddress-ipv4 ()
  "This function creates a ipaddress-ipv4 object"
  (make-instance 'ipaddress-ipv4))

(defun make-network-ipv4 ()
  "This function creates a ipnetwork-ipv4 object"  
  (make-instance 'ipnetwork-ipv4))

(defun make-range-ipv4 ()
  "This function creates an empty ipv4 address range"
  (make-instance 'iprange-ipv4))

(defun make-ipaddress-ipv6 ()
  "This function creates a ipaddress-ipv6 object"
  (make-instance 'ipaddress-ipv6))

(defun make-network-ipv6 ()
  "This function creates a ipnetwork-ipv6 object"  
  (make-instance 'ipnetwork-ipv6))

(defun make-range-ipv6 ()
  "This function creates an empty ipv6 address range"
  (make-instance 'iprange-ipv6))

(defgeneric ipaddress-ip-as-unsigned-byte (ip)
  (:documentation
   "Returns the ip as a unsigned byte, most significant bit is the highest bit")
  (:method ((ipaddress ipaddress-ipv4))
    (slot-value ipaddress 'ip-value))
  (:method ((ipaddress ipaddress-ipv6))
    (slot-value ipaddress 'ip-value)))

(defgeneric (setf ipaddress-ip-as-unsigned-byte) (new-value ip)
  (:documentation
   "Sets the ip as an unsgigned-byte, most significant bit is the highest bit")
  (:method (new-value (ipaddress ipaddress-ipv4))
    (declare (type (unsigned-byte 32) new-value))
    (unless (and (integerp new-value)
		 (<= 0 new-value #xFFFFFFFF))
      (error "~S is not a valid ip as unsigned-byte"
	     new-value))
    (setf (slot-value ipaddress 'ip-value)
	  new-value))
  (:method (new-value (ipaddress ipaddress-ipv6))
    (declare (type (unsigned-byte 128) new-value))
    (unless (and (integerp new-value)
		 (<= 0 new-value #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF))
      (error "~S is not a valid ip as unsigned-byte"
	     new-value))
    (setf (slot-value ipaddress 'ip-value)
	  new-value)))

(defgeneric ipaddress-ip-as-parts (ip)
  (:documentation
   "Returns the ip as a list of parts, most significant bit is the highest bit
For ipv4 we use (unsigned-byte 8), for ipv6 (unsigned-byte 16)")
  (:method ((ipaddress ipaddress-ipv4))
    (loop :for power fixnum :downfrom 24 :by 8 :to 0
	  :with value :of-type (unsigned-byte 32) = (slot-value ipaddress 'ip-value)
	  :collect
	  (ldb (byte 8 power) value)))
  (:method ((ipaddress ipaddress-ipv6))
    (loop :for power fixnum :downfrom 112 :by 16 :to 0
	  :with value :of-type (unsigned-byte 128) = (slot-value ipaddress 'ip-value)
	  :collect
	  (ldb (byte 16 power) value))))

(defgeneric (setf ipaddress-ip-as-parts) (new-value ip)
  (:documentation
   "Sets the ip as a list of parts, starting with the most significant
For ipv4 we use (unsigned-byte 8), for ipv6 (unsigned-byte 16)")
  (:method (new-value (ipaddress ipaddress-ipv4))
    (unless (listp new-value)
      (error "~S is not a legal value to set the ip octets of ~S to"
	     new-value
	     ipaddress))
    (loop :with new-ip :of-type (unsigned-byte 32) = 0
	  :for power fixnum :downfrom 24 :by 8
	  :for value :of-type (unsigned-byte 8) :in new-value
	  :do
	  (when (< power 0)
	    (error "Too many octets: ~S"
		   new-value))
	  (unless (and (integerp value)
		       (<= 0 value 255))
	    (error "Octet ~A is not compatible"
		   value))
	  (setf (ldb (byte 8 power) new-ip)
		value)
	  :finally
	  (setf (slot-value ipaddress 'ip-value)
		new-ip))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv6))
    (unless (listp new-value)
      (error "~S is not a legal value to set the ip octets of ~S to"
	     new-value
	     ipaddress))
    (loop :with new-ip :of-type (unsigned-byte 128) = 0
	  :for power fixnum :downfrom 112 :by 16
	  :for value :of-type (unsigned-byte 16) :in new-value
	  :do
	  (when (< power 0)
	    (error "Too many octets: ~S"
		   new-value))
	  (unless (and (integerp value)
		       (<= 0 value #xFFFF))
	    (error "Octet ~A is not compatible"
		   value))
	  (setf (ldb (byte 16 power) new-ip)
		value)
	  :finally
	  (setf (slot-value ipaddress 'ip-value)
		new-ip))
    new-value))

(defgeneric ipaddress-mask-as-unsigned-byte (ip)
  (:documentation
   "Returns the subnet mask as an unsigned byte
This is the traditional subnet mask, not the cisco one")
  (:method ((ipnetwork ipnetwork-ipv4))
    (with-slots (mask-length)
	ipnetwork
       (mask-field
	(byte mask-length (- 32 mask-length))
	#xFFFFFFFF)))
  (:method ((ipaddress ipaddress-ipv4))
    ;; normal ips have a /32
    #xFFFFFFFF)
  (:method ((ipnetwork ipnetwork-ipv6))
    (with-slots (mask-length)
	ipnetwork
       (mask-field
	(byte mask-length (- 128 mask-length))
	#xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)))
  (:method ((ipaddress ipaddress-ipv6))
    ;; normal ips have a /32
    #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF))

(defgeneric (setf ipaddress-mask-as-unsigned-byte) (new-value ip)
  (:documentation
   "Sets the subnet mask as an unsigned byte
This is the traditional subnet mask, not the cisco one")
  (:method (new-value (ipnetwork ipnetwork-ipv4))
    (declare (type (unsigned-byte 32) new-value))
    (let* ((new-length
	    ;; optimistically assume it is right
	    ;; logcount will count the number of 1's in the binary
	    ;; representation of the mask
	    (logcount new-value))
	   (gives-value
	    (mask-field
	     (byte new-length (- 32 new-length))
	     #xFFFFFFFF)))
      (unless (and (= new-value
		      gives-value)
		   (<= 0 new-length 32))
	(error "The new mask ~D becomes the illegal mask length ~A -> ~8,'0X"
	       new-value
	       new-length
	       gives-value))
      (setf (ipaddress-mask-as-length ipnetwork)
	    new-length))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv4))
    (declare (type (unsigned-byte 32) new-value))
    (change-class ipaddress 'ipnetwork-ipv4)
    (setf (ipaddress-mask-as-unsigned-byte ipaddress)
	  new-value))
  (:method (new-value (ipnetwork ipnetwork-ipv6))
    (declare (type (unsigned-byte 128) new-value))
    (let* ((new-length
	    ;; optimistically assume it is right
	    ;; logcount will count the number of 1's in the binary
	    ;; representation of the mask
	    (logcount new-value))
	   (gives-value
	    (mask-field
	     (byte new-length (- 128 new-length))
	     #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)))
      (unless (and (= new-value
		      gives-value)
		   (<= 0 new-length 128))
	(error "The new mask ~D becomes the illegal mask length ~A -> ~8,'0X"
	       new-value
	       new-length
	       gives-value))
      (setf (ipaddress-mask-as-length ipnetwork)
	    new-length))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv6))
    (declare (type (unsigned-byte 128) new-value))
    (change-class ipaddress 'ipnetwork-ipv6)
    (setf (ipaddress-mask-as-unsigned-byte ipaddress)
	  new-value)))

(defgeneric ipaddress-cisco-mask-as-unsigned-byte (ip)
  (:documentation
   "Returns the subnet mask as an unsigned byte
This is the traditional cisco subnet mask.")
  (:method ((ipnetwork ipnetwork-ipv4))
    (with-slots (mask-length)
	ipnetwork
      (mask-field
	(byte (- 32 mask-length) 0)
	#xFFFFFFFF)))
  (:method ((ipaddress ipaddress-ipv4))
    ;; normal ips have a /32
    0)
  (:method ((ipnetwork ipnetwork-ipv6))
    (with-slots (mask-length)
	ipnetwork
      (mask-field
	(byte (- 128 mask-length) 0)
	#xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)))
  (:method ((ipaddress ipaddress-ipv6))
    ;; normal ips have a /128
    0))

(defgeneric (setf ipaddress-cisco-mask-as-unsigned-byte) (new-value ip)
  (:documentation
   "Sets the subnet mask as an unsigned byte
This is the cisco subnet mask.")
  (:method (new-value (ipnetwork ipnetwork-ipv4))
    (declare (type (unsigned-byte 32) new-value))
    (let* ((new-length
	    ;; optimistically assume it is right
	    ;; logcount will count the number of 1's in the binary
	    ;; representation of the mask
	    (- 32 (logcount new-value)))
	   (gives-value
	    (mask-field
	     (byte (- 32 new-length) 0)
	     #xFFFFFFFF)))
      (unless (and (= new-value
		      gives-value)
		   (<= 0 new-length 32))
	(error "The new cisco mask ~X becomes the illegal mask length ~A -> ~8,'0X"
	       new-value
	       new-length
	       gives-value))
      (setf (ipaddress-mask-as-length ipnetwork)
	    new-length))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv4))
    (declare (type (unsigned-byte 32) new-value) )
    (change-class ipaddress 'ipnetwork-ipv4)
    (setf (ipaddress-cisco-mask-as-unsigned-byte ipaddress)
	  new-value))
  (:method (new-value (ipnetwork ipnetwork-ipv6))
    (declare (type (unsigned-byte 128) new-value))
    (let* ((new-length
	    ;; optimistically assume it is right
	    ;; logcount will count the number of 1's in the binary
	    ;; representation of the mask
	    (- 128 (logcount new-value)))
	   (gives-value
	    (mask-field
	     (byte (- 128 new-length) 0)
	     #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)))
      (unless (and (= new-value
		      gives-value)
		   (<= 0 new-length 128))
	(error "The new cisco mask ~X becomes the illegal mask length ~A -> ~8,'0X"
	       new-value
	       new-length
	       gives-value))
      (setf (ipaddress-mask-as-length ipnetwork)
	    new-length))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv6))
    (declare (type (unsigned-byte 128) new-value) )
    (change-class ipaddress 'ipnetwork-ipv6)
    (setf (ipaddress-cisco-mask-as-unsigned-byte ipaddress)
	  new-value)))

(defgeneric ipaddress-mask-as-length (ip)
  (:documentation
   "Returns the length of the subnet mask of the ip")
  (:method ((ipnetwork ipnetwork-ipv4))
    (slot-value ipnetwork
		'mask-length))
  (:method ((ipaddress ipaddress-ipv4))
    ;; normal ips have a /32
    32)
  (:method ((ipnetwork ipnetwork-ipv6))
    (slot-value ipnetwork
		'mask-length))
  (:method ((ipaddress ipaddress-ipv6))
    ;; normal ips have a /32
    128))

(defgeneric (setf ipaddress-mask-as-length) (new-value ip)
  (:documentation
   "Sets the length of the subnet mask of the ip")
  (:method (new-value (ipnetwork ipnetwork-ipv4))
    (declare (type (integer 0 32) new-value))
    (unless (<= 0 new-value 32)
      (error "Illegual mask length ~S"
	     new-value))
    (setf (slot-value ipnetwork
		      'mask-length)
	  new-value))
  (:method (new-value (ipaddress ipaddress-ipv4))
    (declare (type (integer 0 32) new-value))
    (change-class ipaddress 'ipnetwork-ipv4)
    (setf (ipaddress-mask-as-length ipaddress)
	  new-value))
  (:method (new-value (ipnetwork ipnetwork-ipv6))
    (declare (type (integer 0 128) new-value))
    (unless (<= 0 new-value 128)
      (error "Illegual mask length ~S"
	     new-value))
    (setf (slot-value ipnetwork
		      'mask-length)
	  new-value))
  (:method (new-value (ipaddress ipaddress-ipv6))
    (declare (type (integer 0 128) new-value))
    (change-class ipaddress 'ipnetwork-ipv6)
    (setf (ipaddress-mask-as-length ipaddress)
	  new-value)))


(defgeneric ipaddress-mask-as-parts (ip)
  (:documentation
   "Returns the mask as a list of octets starting with the most significant octet
This is the traditional subnet mask, not the cisco one")
  (:method ((ipaddress ipaddress-ipv4))
    (list 255 255 255 255))
  (:method ((ipaddress ipnetwork-ipv4))
    (loop :for power fixnum :downfrom 24 :by 8 :to 0
	  :with value :of-type (unsigned-byte 32) = (ipaddress-mask-as-unsigned-byte ipaddress)
	  :collect
	  (ldb (byte 8 power) value)))
  (:method ((ipaddress ipaddress-ipv6))
    (list #xFFFF #xFFFF #xFFFF #xFFFF
	  #xFFFF #xFFFF #xFFFF #xFFFF))
  (:method ((ipaddress ipnetwork-ipv6))
    (loop :for power fixnum :downfrom 112 :by 16 :to 0
	  :with value :of-type (unsigned-byte 128) = (ipaddress-mask-as-unsigned-byte ipaddress)
	  :collect
	  (ldb (byte 16 power) value))))

(defgeneric (setf ipaddress-mask-as-parts) (new-value ip)
  (:documentation
   "Sets the mask as a list of octets starting with the most significant octet
This is the traditional subnet mask, not the cisco one")
  (:method (new-value (ipnetwork ipnetwork-ipv4))
    (loop :with new-mask :of-type (unsigned-byte 32) = 0
	  :for power fixnum :downfrom 24 :by 8
	  :for value :of-type (unsigned-byte 8) :in new-value
	  :do
	  (when (< power 0)
	    (error "Too many octets: ~S"
		   new-value))
	  (unless (and (integerp value)
		       (<= 0 value 255))
	    (error "Octet ~A is not compatible"
		   value))
	  (setf (ldb (byte 8 power) new-mask)
		value)
	  :finally
	  (setf (ipaddress-mask-as-unsigned-byte ipnetwork)
		new-mask))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv4))
    (change-class ipaddress 'ipnetwork-ipv4)
    (setf (ipaddress-mask-as-parts ipaddress)
	  new-value))
  (:method (new-value (ipnetwork ipnetwork-ipv6))
    (loop :with new-mask :of-type (unsigned-byte 128) = 0
	  :for power fixnum :downfrom 112 :by 16
	  :for value :of-type (unsigned-byte 16) :in new-value
	  :do
	  (when (< power 0)
	    (error "Too many octets: ~S"
		   new-value))
	  (unless (and (integerp value)
		       (<= 0 value #xFFFF))
	    (error "Octet ~A is not compatible"
		   value))
	  (setf (ldb (byte 16 power) new-mask)
		value)
	  :finally
	  (setf (ipaddress-mask-as-unsigned-byte ipnetwork)
		new-mask))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv6))
    (change-class ipaddress 'ipnetwork-ipv6)
    (setf (ipaddress-mask-as-parts ipaddress)
	  new-value)))

(defgeneric ipaddress-cisco-mask-as-parts (ip)
  (:documentation
   "Returns the mask as a list of octets starting with the most significant octet
This is the traditional subnet mask, not the cisco one")
  (:method ((ipaddress ipaddress-ipv4))
    (list 0 0 0 0))
  (:method ((ipaddress ipnetwork-ipv4))
    (loop :for power fixnum :downfrom 24 :by 8 :to 0
	  :with value :of-type (unsigned-byte 32) = (ipaddress-cisco-mask-as-unsigned-byte ipaddress)
	  :collect
	  (ldb (byte 8 power) value)))
  (:method ((ipaddress ipaddress-ipv6))
    (list 0 0 0 0
	  0 0 0 0))
  (:method ((ipaddress ipnetwork-ipv6))
    (loop :for power fixnum :downfrom 112 :by 16 :to 0
	  :with value :of-type (unsigned-byte 128) = (ipaddress-cisco-mask-as-unsigned-byte ipaddress)
	  :collect
	  (ldb (byte 16 power) value))))

(defgeneric (setf ipaddress-cisco-mask-as-parts) (new-value ip)
  (:documentation
   "Sets the mask as a list of octets starting with the most significant octet
This is the traditional subnet mask, not the cisco one")
  (:method (new-value (ipnetwork ipnetwork-ipv4))
    (loop :with new-mask :of-type (unsigned-byte 32) = 0
	  :for power fixnum :downfrom 24 :by 8
	  :for value :of-type (unsigned-byte 8) :in new-value
	  :do
	  (when (< power 0)
	    (error "Too many octets: ~S"
		   new-value))
	  (unless (and (integerp value)
		       (<= 0 value 255))
	    (error "Octet ~A is not compatible"
		   value))
	  (setf (ldb (byte 8 power) new-mask)
		value)
	  :finally
	  (setf (ipaddress-cisco-mask-as-unsigned-byte ipnetwork)
		new-mask))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv4))
    (change-class ipaddress 'ipnetwork-ipv4)
    (setf (ipaddress-cisco-mask-as-parts ipaddress)
	  new-value))
  (:method (new-value (ipnetwork ipnetwork-ipv6))
    (loop :with new-mask :of-type (unsigned-byte 128) = 0
	  :for power fixnum :downfrom 112 :by 16
	  :for value :of-type (unsigned-byte 16) :in new-value
	  :do
	  (when (< power 0)
	    (error "Too many octets: ~S"
		   new-value))
	  (unless (and (integerp value)
		       (<= 0 value #xFFFF))
	    (error "Octet ~A is not compatible"
		   value))
	  (setf (ldb (byte 16 power) new-mask)
		value)
	  :finally
	  (setf (ipaddress-cisco-mask-as-unsigned-byte ipnetwork)
		new-mask))
    new-value)
  (:method (new-value (ipaddress ipaddress-ipv6))
    (change-class ipaddress 'ipnetwork-ipv6)
    (setf (ipaddress-cisco-mask-as-parts ipaddress)
	  new-value)))

(defgeneric ipaddress-ip-to-string (ip)
  (:documentation
   "Prints the ip in a 'convenient' format")
  (:method ((ipaddress ipaddress-ipv4))
    (format nil "~{~D~^.~}"
	    (ipaddress-ip-as-parts ipaddress)))
  (:method ((ipaddress ipnetwork-ipv4))
    (format nil "~{~D~^.~}/~D"
	    (ipaddress-ip-as-parts ipaddress)
	    (ipaddress-mask-as-length ipaddress)))
  ;;; ipv6
  (:method ((ipaddress ipaddress-ipv6))
    (format nil "~{~X~^:~}"
	    (ipaddress-ip-as-parts ipaddress)))
  (:method ((ipaddress ipnetwork-ipv6))
    (format nil "~{~X~^:~}/~D"
	    (ipaddress-ip-as-parts ipaddress)
	    (ipaddress-mask-as-length ipaddress))))

(defgeneric ipaddress-mask-format-string (ip)
  (:documentation
   "Returns the format string to print the mask in the 'natural' format
as parameter you should give the ipaddress-ip-as-parts result.")
  (:method ((ipaddress ipaddress-ipv4))
    "~{~D~^.~}")
  (:method ((ipaddress ipaddress-ipv6))
    "~{~4,'0X~^.~}"))
    

(defgeneric (setf iprange-start-range) (new-start range)
  (:documentation
   "Sets the ip at the start of the range")
  (:method ((new-start ipaddress-ipv4) (range iprange-ipv4))
    (setf (ipaddress-ip-as-unsigned-byte range)
	  (ipaddress-ip-as-unsigned-byte new-start))
    new-start)
  (:method ((new-start ipaddress-ipv6) (range iprange-ipv6))
    (setf (ipaddress-ip-as-unsigned-byte range)
	  (ipaddress-ip-as-unsigned-byte new-start))
    new-start))



(defgeneric %iprange-start-range-ip (range)
  (:documentation
   "Returns the ip at the start of the range and an unsigned-byte.")
  (:method ((range iprange-ipv4))
    (ipaddress-ip-as-unsigned-byte
     range))
  (:method ((ip ipaddress-ipv4))
    (ipaddress-ip-as-unsigned-byte
     ip))
  (:method ((network ipnetwork-ipv4))
    (logand (ipaddress-mask-as-unsigned-byte network)
	    (ipaddress-ip-as-unsigned-byte network)))
  (:method ((range iprange-ipv6))
    (ipaddress-ip-as-unsigned-byte
     range))
  (:method ((ip ipaddress-ipv6))
    (ipaddress-ip-as-unsigned-byte
     ip))
  (:method ((network ipnetwork-ipv6))
    (logand (ipaddress-mask-as-unsigned-byte network)
	    (ipaddress-ip-as-unsigned-byte network))))

(defgeneric iprange-start-range (range)
  (:documentation
   "Returns the ip at the start of the range.
The netmask is kept")
  (:method ((range iprange-ipv4))
    range)
  (:method ((ip ipaddress-ipv4))
    ip)
  (:method ((network ipnetwork-ipv4))
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-length network))
      ;; now clear out the masked out bits:
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    (%iprange-start-range-ip network))
      ip))
  (:method ((range iprange-ipv6))
    range)
  (:method ((ip ipaddress-ipv6))
    ip)
  (:method ((network ipnetwork-ipv6))
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-length network))
      ;; now clear out the masked out bits:
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    (%iprange-start-range-ip network))
      ip)))


(defgeneric %iprange-end-range-ip (range)
  (:documentation
   "Returns the ip at the end of the range as an unsigned-byte.")
  (:method ((range iprange-ipv4))
    (ipaddress-ip-as-unsigned-byte
     (slot-value range 'end-range)))
  (:method ((ip ipaddress-ipv4))
    (ipaddress-ip-as-unsigned-byte
     ip))
  (:method ((network ipnetwork-ipv4))
    (logior (logxor
	     (ipaddress-mask-as-unsigned-byte network)
	     #xFFFFFFFF)
	    (ipaddress-ip-as-unsigned-byte network)))
  (:method ((range iprange-ipv6))
    (ipaddress-ip-as-unsigned-byte
     (slot-value range 'end-range)))
  (:method ((ip ipaddress-ipv6))
    (ipaddress-ip-as-unsigned-byte
     ip))
  (:method ((network ipnetwork-ipv6))
    (logior (logxor
	     (ipaddress-mask-as-unsigned-byte network)
	     #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
	    (ipaddress-ip-as-unsigned-byte network))))

(defgeneric iprange-end-range (range)
  (:documentation
   "Returns the ip at the end of the range.
The netmask is kept")
  (:method ((range iprange-ipv4))
    (slot-value range 'end-range))
  (:method ((ip ipaddress-ipv4))
    ip)
  (:method ((network ipnetwork-ipv4))
    (let ((ip (make-ipaddress-ipv4)))
      (setf (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-length network))
      ;; now clear out the masked out bits:
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    (%iprange-end-range-ip network))
      ip))
  (:method ((range iprange-ipv6))
    (slot-value range 'end-range))
  (:method ((ip ipaddress-ipv6))
    ip)
  (:method ((network ipnetwork-ipv6))
    (let ((ip (make-ipaddress-ipv6)))
      (setf (ipaddress-mask-as-length ip)
	    (ipaddress-mask-as-length network))
      ;; now clear out the masked out bits:
      (setf (ipaddress-ip-as-unsigned-byte ip)
	    (%iprange-end-range-ip network))
      ip)))

(defgeneric (setf iprange-end-range) (new-start range)
  (:documentation
   "Sets the ip at the ends of the range")
  (:method ((new-end ipaddress-ipv4) (range iprange-ipv4))
    (setf (slot-value range 'end-range)
	  new-end)
    new-end)
  (:method ((new-end ipaddress-ipv6) (range iprange-ipv6))
    (setf (slot-value range 'end-range)
	  new-end)
    new-end))

;;; parse ip
(defun parse-ipv4 (string)
  "Parses a given ip, supported formats:
127.0.0.1
0x7F000001
with masks:
127.0.0.1/255.0.0.0
127.0.0.1/8
127.0.0.1/0xFF000000
0x7F000001/0xFF000000
also with cisco masks:
127.0.0.1/0.255.255.255
0x7F000001/00FFFFFF
"
  (let* ((parts
	  (split-sequence:split-sequence #\/ string))
	 (host-part (when parts
		      (first parts)))
	 (mask-part (when parts
		      (second parts)))
	 (ip (make-ipaddress-ipv4)))
    ;;; it is a list of octects or
    ;;; a hex number?
    (cond
      ((and (char= #\0 (aref host-part 0))
	    (char= #\x (aref host-part 1))
	    (let ((addr (parse-integer
			 host-part
			 :start 2
			 :end (length host-part)
			 :radix 16)))
	      (when addr
		(setf (ipaddress-ip-as-unsigned-byte ip)
		      addr)
		t)))
       t)
      ;; is it a 1.2.3.4
      ((let ((parts (split-sequence:split-sequence #\.
						   host-part)))
	 (when (and parts
		    (= (length parts) 4))
	   (setf (ipaddress-ip-as-parts ip)
		 (loop :for part :in parts
		       :collect (parse-integer part)))
	   t))
       t)
      (t
       (error "unknown ip format: ~S from ~S"
	      host-part
	      string)))
    (when mask-part
      ;;; a hex number?
      (cond
	((and (char= #\0 (aref mask-part 0))
	      (char= #\x (aref mask-part 1))
	      (let ((addr (parse-integer
			   mask-part
			   :start 2
			   :end (length mask-part)
			   :radix 16)))
		(when addr
		  ;; is a normal mask?
		  (if (= 1 (ldb (byte 1 31) addr))
		      (setf (ipaddress-mask-as-unsigned-byte ip)
			    addr)
		      (setf (ipaddress-cisco-mask-as-unsigned-byte ip)
			    addr))
		  t)))
	 t)
	;; is it a 1.2.3.4
	((let ((parts (split-sequence:split-sequence #\.
						     mask-part)))
	   (when (and parts
		      (= (length parts) 4))
	     (let ((parts (loop :for part :in parts
				:collect (parse-integer part))))
	       ;; is a normal mask?
	       (if (= 1 (ldb (byte 1 7) (first parts)))
		   (setf (ipaddress-mask-as-parts ip)
			 parts)
		   (setf (ipaddress-cisco-mask-as-parts ip)
			 parts))
	       t)))
	 t)
	;; maybe justa length?
	((let ((length (parse-integer mask-part)))
	   (when (and length
		      (<= 0 length 32))
	     (setf (ipaddress-mask-as-length ip)
		   length)
	     t))
	 t)
	(t
	 (error "unknown mask format: ~S from ~S"
		mask-part
		string))))
    ip))

(defun parse-ipv6 (string)
  "Parses a given ip, supported formats:
x:x:x:x:x:x:x:x
 where x is a (unsigned-byte 16) in hex

compressed format:
x:x:x::x:x where :: is filled with 0's
extreme case:
::

THIS CAN NOT be used to drop trailing 0's!

mixed case:
x:x:x:x:x:x:d.d.d.d where d are the
 traditional ipv6 notations
compression is also possible

with masks:

x:x:x:x:x:x:x:x/length
 where lenght is in decimal
"
  (let* ((parts
	  (split-sequence:split-sequence #\/ string))
	 (host-part (when parts
		      (first parts)))
	 (mask-part (when parts
		      (second parts)))
	 (host-sections
	  (when host-part
	    (let ((host-sections 
		   (split-sequence:split-sequence #\:
						  host-part)))
	      ;; now there can be only one :: part
	      ;; it can be that the whole string is ::
	      (when (equalp host-sections
			    (list "" "" ""))
		(setf host-sections (list :filler)))
	      ;; it can be that we start with ::
	      (when (and (string= (first host-sections) "")
			 (string= (second host-sections) ""))
		(setf host-sections
		      (cons :filler
			    (nthcdr 2 host-sections))))
	      ;; it can be we end with ::
	      (let ((length (length host-sections)))
		(when (and (string= (nth (- length 1)
					 host-sections)
				    "")
			 (string= (nth (- length 2)
				       host-sections)
				  ""))
		  (setf host-sections
			(append (nbutlast host-sections
					  2)
				(list :filler)))))
	      ;; now go and fix the list
	      (loop :for item :in host-sections
		    :with found-filler = nil
		    :collect
		    (progn
		      (if (or (string= item "")
			      (eq item :filler))
			  (if found-filler
			      (error "Found two fillers in ipv6 ip ~S" string)
			      (progn
				(setf found-filler t)
				:filler))
			  item))))))
	 (last-host-section
	  (when host-sections
	    (first
	     (last host-sections))))
	 (ipv4-parts
	  (when (and last-host-section
		     (stringp last-host-section)
		     (find #\. last-host-section :test #'char=))
	    (split-sequence:split-sequence #\.
					   last-host-section)))
	 (ip (make-ipaddress-ipv6)))
    
    ;;; first do the host part
    (setf (ipaddress-ip-as-parts ip)
	  (loop :for section :in host-sections
		:append
		(cond
		  ((eq section :filler)
		   ;; insert the zero's
		   (make-list (- 8
				 (if ipv4-parts
				     1
				     0)
				 (1- (length host-sections)))
			      :initial-element 0))
		  ((and ipv4-parts
			(eq last-host-section
			    section))
		   (list (+ (ash (parse-integer (first ipv4-parts)) 8)
			    (parse-integer (second ipv4-parts)))
			 (+ (ash (parse-integer (third ipv4-parts)) 8)
			    (parse-integer (fourth ipv4-parts)))))
		  (t
		   (list (parse-integer section
					:radix 16))))))
    ;; then the network:
    (when mask-part
      (let ((length (parse-integer mask-part)))
	(setf (ipaddress-mask-as-length ip)
	      length)))
    ip))

(defun parse-ip (string)
  "Tries to parse a string as an ip
uses parse-ipv6 if it sees a #\: in the string
uses parse-ipv4 if it does not see a #\:"
  (if (position #\: string)
      (parse-ipv6 string)
      (parse-ipv4 string)))

(defun parse-range (string)
  "Tries to parse an ip range
Supported format is:
  <ip>-<ip>"
  (let ((parts (split-sequence:split-sequence #\-
					      string)))
    (when (and parts
	       (= 2
		  (length parts)))
      (let* ((start (parse-ip (first parts)))
	     (end (parse-ip (second parts))))
	(if (typep start 'ipaddress-ipv4)
	    (if (typep end 'ipaddress-ipv4)
		(let ((range (make-range-ipv4)))
		  (setf (iprange-start-range range)
			start)
		  (setf (iprange-end-range range)
			end)
		  range)
		(error "the end ip ~S is not ipv4 like the start ~S"
		       end
		       start))
	    (if (typep end 'ipaddress-ipv6)
		(let ((range (make-range-ipv6)))
		  (setf (iprange-start-range range)
			start)
		  (setf (iprange-end-range range)
			end)
		  range)
		(error "the end ip ~S is not ipv6 like the start ~S"
		       end
		       start)))))))

(defgeneric ip-contains-p (container item)
  (:documentation
   "Returns true is the container contains item")
  (:method ((container ipaddress) (item ipaddress))
    (<= (%iprange-start-range-ip
	 container)
	(%iprange-start-range-ip
	 item)
	(%iprange-end-range-ip
	 item)
	(%iprange-end-range-ip
	 container))))

(defgeneric ip-has-overlap-p (a b)
  (:documentation
   "Returns T if the a and b have some overlap")
  (:method ((a ipaddress) (b ipaddress))
    (let ((start-a
	   (%iprange-start-range-ip
	    a))
	  (start-b
	   (%iprange-start-range-ip
	    b))
	  (end-a
	   (%iprange-end-range-ip
	    a))
	  (end-b
	   (%iprange-end-range-ip
	    b)))
      (or
       ;; <---A----<==>==-B===>
       (<= start-a
	   start-b
	   end-a
	   end-b)
       ;; <===<--A-->==B=====>
       (<= start-b
	   start-a
	   end-a
	   end-b)
       ;; <---A---<==B===>-->
       (<= start-a
	   start-b
	   end-b
	   end-a)
       ;; <===B===<-->--A--->
       (<= start-b
	   start-a
	   end-b
	   end-a)))))

(defgeneric ip-in-same-subnet-p (ip-a ip-b)
  (:documentation
   "Returns T if the two ip's are in the same subnet")
  (:method ((a ipnetwork) (b ipnetwork))
    (and (= (%iprange-start-range-ip
	      a)
	    (%iprange-start-range-ip
	      b))
	 (= (%iprange-end-range-ip
	     a)
	    (%iprange-end-range-ip
	     b)))))