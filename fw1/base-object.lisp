;;; -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.fw1)

(defclass fw1-object (item)
  ((type :type string
	 :reader fw1-object-type
	 :initarg :type)
   (color :type string
	  :reader fw1-object-color
	  :initarg :color)
   (comments :type string
	     :reader fw1-object-comments
	     :initarg :comments))
  (:documentation
   "Basic FW1 object with basic properties"))

(defclass fw1-any-object (fw1-object)
  ()
  (:documentation
   "The ANY object"))

(defclass fw1-original-object (fw1-object)
  ()
  (:documentation
   "The ORIGINAL object"))

(defmethod %contains-p ((container fw1-any-object) (item fw1-object))
  (declare (ignore container item))   
  t)

(defmethod %has-overlap-p ((container fw1-any-object) (item fw1-object))
  (declare (ignore container item))   
  t)

(defmethod %has-overlap-p ((container fw1-object) (item fw1-any-object))
  (declare (ignore container item))   
  t)

(defclass fw1-container-object (fw1-object container)
  ()
  (:documentation
   "Basic fw1 container object, no direct instances known"))

(defclass fw1-group-object (fw1-container-object)
  ()
  (:documentation
   "A normal group"))

(defclass fw1-group-with-exclusions-object (fw1-container-object)
  ()
  (:documentation
   "A group with exclusions"))

