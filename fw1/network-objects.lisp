;;; -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.fw1)

(defclass network-object (fw1-object)
  ()
  (:documentation
   "The basic object where all network objects are children of this"))
  
(defclass host (network-object)
  ()
  (:documentation
   "A basic host object, without ip"))

(defclass object-with-ip (host)
  ((ip :type ipaddress
       :accessor host-ip
       :documentation
       "The ip of the host"))
  (:documentation
   "A object with an ip"))

(defclass host-with-ip (object-with-ip)
  ()
  (:documentation
   "A mere host with an ip"))

(defclass network-interface ()
  ((name :type string
	 :reader network-interface-name
	 :initarg :name)
   (comments :type string
	     :reader network-interface-comments
	     :initarg :comments)
   (description :type string
		:reader network-interface-description
		:initarg :description)
   (dynamic-ip :type boolean
	       :reader network-interface-dynamic-ip
	       :initarg :dynamic-ip)
   (ifindex :type string
	    :reader network-interface-ifindex
	    :initarg :ifindex)
   (ipaddress :type ip-network
	      :reader network-interface-ipaddress
	      :initarg :ipaddress)
   (officialname :type string
		 :reader network-interface-officialname
		 :initarg :officialname)
   (has-anti-spoof-p :type boolean
		     :initform nil
		     :reader network-interface-has-anti-spoof-p
		     :initarg :has-anti-spoof-p)
   (allowed-traffic :type (or string network-object nil)
		    :initform nil
		    :reader network-interface-allowed-traffic
		    :initarg :allowed-traffic)
   (is-cluster-interface-p :type boolean
			   :initform nil
			   :reader network-interface-is-cluster-interface-p
			   :initarg :is-cluster-interface-p)
   (is-primary-p :type boolean
		 :initform nil
		 :reader network-interface-is-primary-p
		 :initarg :is-primary-p))
  (:documentation
   "This is an interface of an object"))

(defclass host-with-multiple-ip (host-with-ip)
  ((interfaces :type list
	       :reader host-interfaces
	       :initform nil))
  (:documentation
   "A mere host with an ip"))


(defclass gateway-host (host-with-multiple-ip fw1-group-object)
  ()
  (:documentation
   "A gateway"))

(defclass firewall-host (host-with-multiple-ip)
  ()
  (:documentation
   "A firewall"))

(defclass oss-host (host-with-multiple-ip)
  ()
  (:documentation
   "A oss object, most likely a router"))

(defclass fw1-range-object (fw-container-object)
  ((ip-range :type iprange
	     :reader host-ip-range))
  (:documentation
   "A basic ip range object"))

	     




  
  
  

  