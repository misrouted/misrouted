;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :common-lisp-user)

(defpackage :pvaneynd.fw1
  (:documentation
   "This implements stuff to read and handle checkpoint
fw-1 related stuff")
  (:use :common-lisp
	:pvaneynd.matryoshka
	:pvaneynd.ip)
  (:export #:host
	   ))
