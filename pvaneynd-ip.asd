;;; -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :asdf)

(defsystem :pvaneynd-ip
  :name "pvanend-IP"
  :author "Peter Van Eynde"
  :version "1"
  :maintainer "Peter Van Eynde"
  :licence "Lisp Lesser General Public License (LLGPL)"
  :description "ip handling functions"
  :depends-on
  ( :fiveam
    :split-sequence)
  :components
  ((:module
    "ip"
    :components ((:file "package")
		 (:file "ip" :depends-on ("package"))
		 (:file "ip-tests" :depends-on ("ip"))))))


#|
(let ((5am:*debug-on-failure* t) (5am:*debug-on-error* t)) (5am:run!))
|#