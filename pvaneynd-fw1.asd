;;; -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :asdf)

(defsystem :pvaneynd-fw1
  :name "pvanend-fw1"
  :author "Peter Van Eynde"
  :version "1"
  :maintainer "Peter Van Eynde"
  :licence "Lisp Lesser General Public License (LLGPL)"
  :description "fw1 specific method and data-structures"
  :depends-on
  ( :pvaneynd-ip
    :pvaneynd-matryoshka)
  :components
  ((:module
    "fw1"
    :components ((:file "package")
		 (:file "base-object" :depends-on ("package"))
		 (:file "network-objects" :depends-on ("base-object"))
		 (:file "services" :depends-on ("base-object"))))))
