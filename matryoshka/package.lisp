;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :common-lisp-user)

(defpackage :pvaneynd.matryoshka
  (:documentation
   "This implements a system of namespaces that contain
items. Some items are containers which can contain other
items. Special care is taken to provide for fast operations
and resistance to toxic situations like containers that
have subcontainers that contain the container again")
  (:use :common-lisp)
  (:export #:namespace
	   #:namespace-id-to-item
	   #:namespace-name-to-item
	   #:namespace-containers
	   #:namespace-with-ip
	   #:namespace-ip-to-items
	   
	   #:namespace-items
	   
	   #:item
	   #:item-name
	   #:item-namespace
	   #:item-id
	   #:item-contained-in
	   

	   #:container
	   #:container-contains
	   #:container-has-overlap-with

	   #:container-of-listable-items
	   #:container-item-names
	   #:container-excluded-item-names
	   #:container-items
	   #:container-excluded-items
	   
	   #:is-container-p
	   #:find-for-all-items-in
	   #:find-for-all-excluded-items-in
	   #:find-for-all-containing-items-in
	   #:find-for-all-containers-in
	   #:find-for-all-containers-of

	   #:add-to-namespace
	   #:get-item-by
	   #:get-item-if

	   #:add-name-to-container
	   #:add-excluded-name-to-container
	   #:add-item-to-container
	   #:add-excluded-item-to-container
	   
	   #:finalize-object
	   #:finalize-namespace
	   
	   #:contains-p
	   #:%contains-p
	   #:has-overlap-p
	   #:%has-overlap-p

	   #:get-all-indirect-items-for
	   #:get-all-indirect-items-hash-for
	   #:remove-overlap-with-expanded-hash!
	   #:get-overlap-hash-between

	   ))