;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.matryoshka)

(eval-when (:compile-toplevel
	    :load-toplevel
	    :execute)
  (unless (5am:get-test :pvaneynd.matryoshka.suite)
    (5am:def-suite :pvaneynd.matryoshka.suite
	:description "Test matryoashka container functions")))

(5am:in-suite :pvaneynd.matryoshka.suite)

(5am:test item-creation-and-identity
  "Tests basic object creation and identity"
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (namespace (make-instance 'namespace)))
     (eq (add-to-namespace item-a namespace)
	 item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (eq (add-to-namespace item-b namespace)
	 item-a)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (finalize-namespace namespace)
     (eq (gethash "a" (namespace-name-to-item
		       namespace))
	 item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (gethash "b" (namespace-name-to-item
		       namespace))
	 item-a)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (gethash "c" (namespace-name-to-item
		       namespace))
	 nil)))

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-by namespace "a")
	 item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-by namespace "b")
	 item-a)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-by namespace "c")
	 nil)))

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-by namespace (item-id item-a))
	 item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-by namespace (item-id item-b))
	 item-a)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-by namespace 953020)
	 nil)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (first (get-item-if namespace
		      (lambda (item)
			(string= (item-name item)
				 (item-name item-a)))))
	 item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (first (get-item-if namespace
		      (lambda (item)
			(string= (item-name item)
				 (item-name item-a)))))
	 item-b)))
 (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (eq (get-item-if namespace
		      (lambda (item)
			(string= (item-name item)
				 "c")))
	 nil))))

(5am:test group-creation-and-direct-membership
  "Test basic group creating and direct memership"

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-c (make-instance 'container-of-listable-items
				     :name "container-c"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-c namespace)

     (add-name-to-container container-c "a")
     (finalize-namespace namespace)
     (contains-p container-c item-a)))

  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-c (make-instance 'container-of-listable-items
				     :name "container-c"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-c namespace)

     (add-name-to-container container-c "a")
     (finalize-namespace namespace)
     (contains-p container-c item-b)))


  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-c (make-instance 'container-of-listable-items
				     :name "container-c"))
	  (container-d (make-instance 'container-of-listable-items
				     :name "container-d"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-c namespace)
     (add-to-namespace container-d namespace)

     (add-name-to-container container-c "container-d")
     (add-name-to-container container-d "a")
     (finalize-namespace namespace)
     (contains-p container-c item-a)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-c (make-instance 'container-of-listable-items
				     :name "container-c"))
	  (container-d (make-instance 'container-of-listable-items
				     :name "container-d"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-c namespace)
     (add-to-namespace container-d namespace)

     (add-name-to-container container-c "container-d")
     (add-name-to-container container-d "a")
     (finalize-namespace namespace)
     (contains-p container-c container-d)))

  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-c (make-instance 'container-of-listable-items
				     :name "container-c"))
	  (container-d (make-instance 'container-of-listable-items
				     :name "container-d"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-c namespace)
     (add-to-namespace container-d namespace)

     (add-name-to-container container-c "container-d")
     (add-name-to-container container-d "a")
     (finalize-namespace namespace)
     (contains-p container-c item-b))))


(5am:test group-with-excludes
  "Test basic group creating and direct memership"

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-excluded-name-to-container container-a "b")
     (finalize-namespace namespace)
     (contains-p container-a item-a)))

  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-excluded-name-to-container container-a "b")
     (finalize-namespace namespace)
     (contains-p container-a item-b)))


  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-b "c")
     (add-name-to-container container-a "container-b")
     (add-excluded-name-to-container container-a "b")
     (finalize-namespace namespace)
     (contains-p container-a item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-b "c")
     (add-name-to-container container-a "container-b")
     (add-excluded-name-to-container container-a "b")
     (finalize-namespace namespace)
     (contains-p container-a item-b))))

(5am:test groups-and-overlap
  "Test basic group overlaps"

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)
     (has-overlap-p container-a item-a)))

  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)
     (has-overlap-p container-a item-b)))



  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-b "c")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (has-overlap-p container-a container-b)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-a "c")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (has-overlap-p container-a container-b)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-a "c")
     (finalize-namespace namespace)
     (has-overlap-p container-a container-b)))

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (has-overlap-p item-a container-a)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (has-overlap-p container-a item-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)
     (has-overlap-p item-b container-a)))
  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)
     (has-overlap-p container-a item-b)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-a "container-b")
     (finalize-namespace namespace)
     (has-overlap-p container-a item-b)))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (add-name-to-container container-a "container-b")
     (finalize-namespace namespace)
     (has-overlap-p item-b container-b))))



(5am:test find-methods
  "tests the find methods"

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list item-a item-b container-a)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-items-in namespace
			      (lambda (item)
				(if (gethash item to-touch)
				    (remhash item to-touch)
				    (error "I should not touch ~S"
					   item))
				nil))
       (= (hash-table-count to-touch)
	  0))))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list item-a)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-items-in container-a
			      (lambda (item)
				(if (gethash item to-touch)
				    (remhash item to-touch)
				    (error "I should not touch ~S"
					   item))
				nil))
       (= (hash-table-count to-touch)
	  0))))

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-excluded-name-to-container container-a "b")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list item-b)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-excluded-items-in container-a
				       (lambda (item)
					 (if (gethash item to-touch)
					     (remhash item to-touch)
					     (error "I should not touch ~S"
						    item))
					 nil))
       (= (hash-table-count to-touch)
	  0))))

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "container-b")
     (add-name-to-container container-b "b")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list item-a item-b container-a container-b)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-containing-items-in container-a
					 (lambda (item)
					   (if (gethash item to-touch)
					       (remhash item to-touch)
					       (error "I should not touch ~S"
						      item))
					   nil))
       (= (hash-table-count to-touch)
	  0))))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "container-b")
     (add-name-to-container container-b "b")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list item-a container-b)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-items-in container-a
					 (lambda (item)
					   (if (gethash item to-touch)
					       (remhash item to-touch)
					       (error "I should not touch ~S"
						      item))
					   nil))
       (= (hash-table-count to-touch)
	  0))))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "container-b")
     (add-name-to-container container-b "b")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list container-a container-b)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-containers-in namespace
				   (lambda (item)
				     (if (gethash item to-touch)
					 (remhash item to-touch)
					 (error "I should not touch ~S"
						item))
				     nil))
       (= (hash-table-count to-touch)
	  0))))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "container-b")
     (add-name-to-container container-b "b")
     (finalize-namespace namespace)

     (let ((to-touch (make-hash-table :test #'eq)))
       (loop :for item :in (list container-b)
	     :do
	     (setf (gethash item to-touch) item))

       (find-for-all-containers-in container-a
				   (lambda (item)
				     (if (gethash item to-touch)
					 (remhash item to-touch)
					 (error "I should not touch ~S"
						item))
				     nil))
       (= (hash-table-count to-touch)
	  0)))))


(5am:test special-functions
  "tests the special functions"

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (let ((expanded (get-all-indirect-items-for container-a)))
       (and (member container-a expanded)
	    (member item-a expanded)
	    (member item-b expanded)))))

  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)
     (let ((expanded (get-all-indirect-items-for container-a)))
       (member item-b expanded))))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (let ((expanded (get-all-indirect-items-hash-for container-a)))
       (and (gethash container-a expanded)
	    (gethash item-a expanded)
	    (gethash item-b expanded)))))

  (5am:is-false
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (finalize-namespace namespace)
     (let ((expanded (get-all-indirect-items-hash-for container-a)))
       (gethash item-b expanded))))


  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (let* ((expanded (get-all-indirect-items-hash-for container-a)))
       (remove-overlap-with-expanded-hash! item-a expanded)
       (and (gethash container-a expanded)
	    (gethash item-b expanded)
	    (not (gethash item-a expanded))))))
  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace container-a namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "b")
     (finalize-namespace namespace)
     (let* ((expanded (get-all-indirect-items-hash-for container-a)))
       (remove-overlap-with-expanded-hash! container-a expanded)
       (and (not (gethash container-a expanded))
	    (not (gethash item-b expanded))
	    (not (gethash item-a expanded))))))

  (5am:is-true
   (let ((item-a (make-instance 'item
				:name "a"))
	 (item-b (make-instance 'item
				:name "b"))
	 (item-c (make-instance 'item
				:name "c"))
	 (container-a (make-instance 'container-of-listable-items
				     :name "container-a"))
	 (container-b (make-instance 'container-of-listable-items
				     :name "container-b"))
	 (namespace (make-instance 'namespace)))
     (add-to-namespace item-a namespace)
     (add-to-namespace item-b namespace)
     (add-to-namespace item-c namespace)
     (add-to-namespace container-a namespace)
     (add-to-namespace container-b namespace)

     (add-name-to-container container-a "a")
     (add-name-to-container container-a "c")
     (add-name-to-container container-b "a")
     (add-name-to-container container-b "b")
     (finalize-namespace namespace)
     (let ((overlap (get-overlap-hash-between container-a
					      container-b)))
       (and (gethash item-a overlap)
	    (not (gethash item-b overlap))
	    (not (gethash item-c overlap))
	    (not (gethash container-a overlap))
	    (not (gethash container-b overlap)))))))



