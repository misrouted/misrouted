;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.matryoshka)

(defun is-container-p (object)
  (typep object 'container))

(defgeneric find-for-all-items-in (namespace-or-container function)
  (:documentation
   "Calls the function with all the items of the given namespace or container until that
function returns a non-nil value, then return that value. otherwise return false")
  (:method ((namespace namespace) function)
    (loop :for item :being :the :hash-value :of
	  (namespace-id-to-item namespace)
	  :thereis (funcall function item)))
  (:method ((container container-of-listable-items) function)
    (loop :for item :being :the :hash-key :of
	  (container-items container)
	  :thereis (funcall function item))))

(defgeneric find-for-all-excluded-items-in (container function)
  (:documentation
   "Calls the function with all the items of the given namespace or container until that
function returns a non-nil value, then return that value. otherwise return false")
  (:method ((container container-of-listable-items) function)
    (loop :for item :being :the :hash-key :of
	  (container-excluded-items container)
	  :thereis (funcall function item))))

(defgeneric find-for-all-containing-items-in (container function)
  (:documentation
   "Calls the function with all the items the given container contains, even indrectly, until that
function returns a non-nil value, then return that value. otherwise return false")
  (:method ((container container) function)
    (loop :for item :being :the :hash-key :of
	  (container-contains container)
	  :thereis (and (gethash item (container-contains container))
			(funcall function item)))))

(defgeneric find-for-all-containers-in (namespace-or-container function)
  (:documentation
   "Calls the function with all the containers of the given namespace or container until that
function returns a non-nil value, then return that value. otherwise return false")
  (:method ((namespace namespace) function)
    (loop :for container :being :the :hash-value :of
	  (namespace-containers namespace)
	  :thereis (funcall function container)))
  (:method ((container container-of-listable-items) function)
    (loop :for item :being :the :hash-key :of
	  (container-items container)
	  :thereis (and (is-container-p item)
			(funcall function item)))))

(defgeneric find-for-all-containers-of (object function)
  (:documentation
   "Calls the function with all the containers of the given object until that
function returns a non-nil value, then return that value. otherwise return false")
  (:method ((object item) function)
    (loop :for container :being :the :hash-key :of
	  (item-contained-in object)
	  :thereis (funcall function container))))

(defgeneric add-to-namespace (item namespace)
  (:documentation
   "Adds an ITEM to a NAMESPACE taking care of all hashes
Returns the item")
  (:method ((item item) (into-namespace namespace))
    (with-slots (maximum-id id-to-item name-to-item)
	into-namespace
      (when (gethash (item-name item)
		     name-to-item)
	(error "An item with name ~A already exists in the namespace ~S, cannot add ~S"
	       (item-name item)
	       into-namespace
	       item))
      (with-slots (id namespace name)
	  item
	(setf id (incf maximum-id))
	(setf (gethash id id-to-item)
	      item)
	(setf (gethash name name-to-item)
	      item)
	(setf namespace
	      into-namespace)
	item)))
  (:method ((item container) (into-namespace namespace))
    (prog1
	(call-next-method)
      (setf (gethash item
		     (slot-value into-namespace
				 'containers))
	    item)))
  (:method ((item item-with-ip) (into-namespace namespace-with-ip))
    (prog1
	(call-next-method)
      (let ((ip (pvaneynd.ip:ipaddress-ip-as-unsigned-byte
		 (item-ipaddress item))))
	(if (gethash ip (namespace-ip-to-items into-namespace))
	    (pushnew item 
		     (gethash ip (namespace-ip-to-items into-namespace)))
	    (setf (gethash ip (namespace-ip-to-items into-namespace))
		  (list item)))))))


(defgeneric get-item-by (namespace identifier)
  (:documentation
   "Returns the item in container that is identified by identifier
returns nil if the object is not found")
  (:method ((namespace namespace) (string string))
    (gethash string
	     (namespace-name-to-item
	      namespace)))
  (:method ((namespace namespace) id)
    (gethash id
	     (namespace-id-to-item
	      namespace))))

(defgeneric get-item-if (namespace test)
  (:documentation
   "Returns the list of objects from the namespace for which the test returned true")
  (:method ((namespace namespace) function)
    (let ((result nil))
      (find-for-all-items-in namespace
			     (lambda (object)
			       (when (funcall function object)
				 (push object result))
			       nil))
      result)))

(defgeneric %contains-p (a b)
  (:documentation
   "Returns T if the item a contains the object b in any way
This is the 'true' function, the result of which is cached for the contains-p
function.")
  (:method ((a item) (b item))
    (eq a b))
  (:method ((a container) (b item))
    (unless (eq (type-of b) 'item)
      (error "Cannot check if ~S contains ~S"
	     a b)))
  (:method ((a item) (b container))
    (unless (eq (type-of a) 'item)
      (error "Cannot check if ~S contains ~S"
	     a b)))
  (:method ((a container) (b container))
    (unless (and (eq (type-of a) 'container)
		 (eq (type-of b) 'container))
      (error "Cannot check if ~S contains ~S"
	     a b)))
  (:method ((a container-of-listable-items) (b item))
    (or (eq a b)
	(and
	 (or (gethash b (container-items a))
	     (find-for-all-items-in a
				    (lambda (item)
				      (contains-p item b))))
	 ;; it is not excluded?
	 (not
	  (or (gethash b (container-excluded-items a))
	      (find-for-all-excluded-items-in a
					      (lambda (item)
						(contains-p item b))))))))
  (:method ((a container-of-listable-items) (b container-of-listable-items))
    (or (call-next-method)
	(not (find-for-all-items-in
	      b
	      (lambda (b-item)
		(not (contains-p a b-item))))))))

(defgeneric add-name-to-container (container item-name)
  (:documentation
   "Adds a item-name to a certain container")
  (:method ((container container-of-listable-items) (item-name string))
    (pushnew item-name
	     (container-item-names container)
	     :test #'string=)))

(defgeneric add-excluded-name-to-container (container item-name)
  (:documentation
   "Adds a item-name to a certain container")
  (:method ((container container-of-listable-items) (item-name string))
    (pushnew item-name
	     (container-excluded-item-names container)
	     :test #'string=)))

(defgeneric add-item-to-container (container item)
  (:documentation
   "Adds a item to a certain container")
  (:method ((container container-of-listable-items) (item item))
    (setf (gethash item (container-items container))
	  item)
    (pushnew (item-name item)
	     (container-item-names container)
	     :test #'string=)))

(defgeneric add-excluded-item-to-container (container item)
  (:documentation
   "Adds a item-name to a certain container")
  (:method ((container container-of-listable-items) (item item))
    (setf (gethash item (container-excluded-items container))
	  item)
    (pushnew (item-name item)
	     (container-excluded-item-names container)
	     :test #'string=)))

(defgeneric finalize-object (object)
  (:documentation
   "Finalised the object, does everything that needs to be done
to actively use the object")
  (:method ((container container))
    nil)
  (:method ((container container-of-listable-items))
    (loop :for name :in (container-item-names container)
	  :with namespace = (item-namespace container)
	  :with item-hash = (container-items container)
	  :for object = (get-item-by namespace
				     name)
	  :do
	  (unless object
	    (error "no object with name ~S found in ~S for container ~S"
		   name
		   namespace
		   (item-name container)))
	  (setf (gethash object item-hash)
		object))
    (loop :for name :in (container-excluded-item-names container)
	  :with namespace = (item-namespace container)
	  :with excluded-item-hash = (container-excluded-items container)
	  :for object = (get-item-by namespace
				     name)
	  :do
	  (unless object
	    (error "no object with name ~S found in ~S for container ~S"
		   name
		   namespace
		   (item-name container)))
	  (setf (gethash object excluded-item-hash)
		object))))

(defgeneric finalize-namespace (namespace)
  (:documentation
   "Finalised the namespace, does everything that needs to be done
to actively use the namespace")
  (:method ((namespace namespace))
    ;; first create all the containers:
    (find-for-all-containers-in namespace
				(lambda (container)
				  (finalize-object container)
				  nil))
    ;; now go over all possible contains:
    (find-for-all-containers-in namespace
				(lambda (container)
				  (find-for-all-items-in namespace
							 (lambda (object)
							   (contains-p container object)
							   nil))
				  nil))))

(defvar *currently-searching-for-contains-in*
  (make-hash-table :test #'eq)
  "This hash table contains the object that are being expanded for evaluating contains-p.
It is used to detect infinite loops")

(defgeneric contains-p (a b)
  (:documentation
   "Returns T if the item a contains the object b in any way
uses the cached result if possible and fills in the cache if
the result was unknown")
  (:method :around ((a item) (b item))
    (when (gethash a *currently-searching-for-contains-in*)
      (error "we want to look if ~S contains ~S, but we already asked this. There must be an infinite loop..."
	     a
	     b))
    (unwind-protect
	 (call-next-method)
      (remhash a *currently-searching-for-contains-in*)))
  (:method ((a item) (b item))
    (%contains-p a b))
  (:method ((a container) (b item))
    (multiple-value-bind (value found-p)
	(gethash b  (container-contains a))
      (if found-p
	  value
	  ;; we don't know if we contain the object:
	  ;; search for it
	  (cond
	    ((%contains-p a b)
	     (setf (gethash b (container-contains a)) t)
	     t)
	    (t
	     (setf (gethash b (container-contains a)) nil)
	     nil))))))

(defgeneric %has-overlap-p (a b)
  (:documentation "returns T if there is any overlap at all between a and b")
  (:method ((a item) (b item))
    (or (contains-p a b)
	(contains-p b a)))
  (:method ((a container) (b item))
    (unless (eq (type-of b) 'item)
      (error "Don't know how to check if ~S has overlap with ~S"
	     a b)))
  (:method ((a item) (b container))
    (unless (eq (type-of a) 'item)
      (error "Don't know how to check if ~S has overlap with ~S"
	     a b)))
  (:method ((a container) (b container))
    (unless (and (eq (type-of a) 'container)
		 (eq (type-of b) 'container))
      (error "Don't know how to check if ~S has overlap with ~S"
	     a b)))
  (:method ((a container-of-listable-items) (b item))
    (or
     (or (contains-p a b)
	(contains-p b a))
     (find-for-all-items-in
      a
      (lambda (item-from-a)
	(has-overlap-p item-from-a b)))))
  (:method ((a item) (b container-of-listable-items))
    (or
     (or (contains-p a b)
	(contains-p b a))
     (find-for-all-items-in
      b
      (lambda (item-from-b)
	(has-overlap-p a item-from-b)))))
  (:method ((a container-of-listable-items) (b container-of-listable-items))
    (or
     (or (contains-p a b)
	 (contains-p b a))
     (find-for-all-items-in
      a
      (lambda (item-from-a)
	(find-for-all-items-in
	 b
	 (lambda (item-from-b)
	   (has-overlap-p item-from-a item-from-b))))))))

(defgeneric has-overlap-p (a b)
  (:documentation "returns T if there is any overlap at all between a and b
uses the cache if possible and fills the cache if the result is not known")
  (:method ((a item) (b item))
    (%has-overlap-p a b))
  (:method ((a container) (b item))
    (multiple-value-bind (value found-p)
	(gethash b  (container-has-overlap-with a))
      (if found-p
	  value
	  (cond
	    ((%has-overlap-p a b)
	     (setf (gethash b (container-has-overlap-with a)) t)
	     t)
	    (t
	     (setf (gethash b (container-has-overlap-with a)) nil)
	     nil)))))
  (:method ((a item) (b container))
    (multiple-value-bind (value found-p)
	(gethash a (container-has-overlap-with b))
      (if found-p
	  value
	  (cond
	    ((%has-overlap-p a b)
	     (setf (gethash a (container-has-overlap-with b)) t)
	     t)
	    (t
	     (setf (gethash a (container-has-overlap-with b)) nil)
	     nil)))))
  (:method ((a container) (b container))
    (multiple-value-bind (value-b b-found-p)
	(gethash a (container-has-overlap-with b))
      (multiple-value-bind (value-a a-found-p)
	  (gethash b (container-has-overlap-with a))

	(cond
	  (a-found-p
	   (unless b-found-p
	     (setf (gethash a (container-has-overlap-with b))
		   value-a))
	   value-a)
	  (b-found-p
	   (unless a-found-p
	     (setf (gethash b (container-has-overlap-with a))
		   value-b))
	   value-b)
	  (t
	   (cond
	    ((%has-overlap-p a b)
	     (setf (gethash a (container-has-overlap-with b)) t)
	     (setf (gethash b (container-has-overlap-with a)) t)
	     t)
	    (t
	     (setf (gethash a (container-has-overlap-with b)) nil)
	     (setf (gethash b (container-has-overlap-with a)) nil)
	     nil))))))))



(defgeneric get-all-indirect-items-for (container)
  (:documentation
   "Returns all items that are known to be contained in the given container")
  (:method ((container container))
    (let ((result nil))
      (find-for-all-containing-items-in container
					(lambda (object)
					  (push object result)
					  nil))
      result)))


(defgeneric get-all-indirect-items-hash-for (container)
  (:documentation
   "Returns a hashtable containing all items that are known to be contained in the given container")
  (:method ((item item))
    (let ((result (make-hash-table :test #'eq)))
      (setf (gethash item result) item)
      result))
  (:method ((container container))
    (let ((result (make-hash-table :test #'eq)))
      (find-for-all-containing-items-in container
					(lambda (object)
					  (setf (gethash object result) object)
					  nil))
      result)))

(defgeneric remove-overlap-with-expanded-hash! (object object-hash)
  (:documentation
   "Removes the object and all indirect objects it might contain from the hash.
Modified and returns the hash")
  (:method ((object item) (object-hash hash-table))
    (remhash object object-hash))
  (:method ((container container) (object-hash hash-table))
    (find-for-all-containing-items-in container
				      (lambda (object)
					(remhash object object-hash)
					nil))
    (remhash container object-hash)))

(defgeneric get-overlap-hash-between (a b)
  (:documentation
   "Retuns the hash containining all objects that are shared between the objects that a and b contain, even indirectly")
  (:method ((a item) (b item))
    (let ((hash (make-hash-table :test #'eq)))
      (when (contains-p a b)
	(setf (gethash a hash) a)
	(setf (gethash b hash) b))
      hash))
  (:method ((a item) (b container))
    (get-overlap-hash-between b a))
  (:method ((a container-of-listable-items) (b item))
    (let ((result (make-hash-table :test #'eq)))
      (when (contains-p a b)
	(setf (gethash b result) b))
      result))
  (:method ((a container-of-listable-items) (b container-of-listable-items))
    (let ((result (make-hash-table :test #'eq)))
      (find-for-all-containing-items-in
       a
       (lambda (object-from-a)
	 (when (contains-p b object-from-a)
	   (setf (gethash object-from-a result) object-from-a))
	 nil))
      (find-for-all-containing-items-in
       b
       (lambda (object-from-b)
	 (when (contains-p a object-from-b)
	   (setf (gethash object-from-b result) object-from-b))
	 nil))
      result)))


