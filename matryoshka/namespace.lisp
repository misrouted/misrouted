;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.matryoshka)

(defclass namespace ()
  ((maximum-id :type (integer 0)
	       :initform 0
	       :reader namespace-maximum-id
	       :documentation
	       "The last id given out, ids go from 1 to this value, inclusive")
   (id-to-item :type hash-table
	       :initform (make-hash-table :test #'eql)
	       :reader namespace-id-to-item
	       :documentation
	       "A hash table on the id of the items, giving the item itself")
   (name-to-item :type hash-table
		 :initform (make-hash-table :test #'equal)
		 :reader namespace-name-to-item
		 :documentation
		 "A hash table on the (unique) names of the items, giving the item itself")
   (containers :type hash-table
	       :initform (make-hash-table :test #'eq)
	       :reader namespace-containers
	       :documentation
	       "A hash table with key and value all the containers of the namespace"))
  (:documentation
   "A namespace is a collection of items with unique names and unique ids"))

(defclass namespace-with-ip (namespace)
  ((ip-to-items :type hash-table
		:initform (make-hash-table :test #'eql)
		:reader namespace-ip-to-items
		:documentation
		"A hashtable with as key the ip of the given items and as value of list of
the items with that ip"))
  (:documentation
   "A namespace where some items can have ips"))



