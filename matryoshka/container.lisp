;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.matryoshka)

(defclass container (item)
  ((contains :type hash-table
	  :initform (make-hash-table :test #'eql)
	  :reader container-contains
	  :documentation
	  "The objects this container indirectly or
directly contains")
   (has-overlap-with :type hash-table
	  :initform (make-hash-table :test #'eql)
	  :reader container-has-overlap-with
	  :documentation
	  "The objects this container has
some overlap with or directly contains"))
  (:documentation
   "The basic container contains objects or can have overlap"))

(defclass container-of-listable-items (container)
  ((item-names :type list
	       :initform nil
	       :accessor container-item-names
	       :documentation
	       "The names of the items the container contains.
Often we don't have the real objects yet, we have to wait
until the namespace gets finalized")
   (excluded-item-names :type list
			:initform nil
			:accessor container-excluded-item-names
			:documentation
			"The names of the items the container excludes.
Often we don't have the real objects yet, we have to wait
until the namespace gets finalized")
   (items :type hash-table
	  :initform (make-hash-table :test #'eql)
	  :reader container-items
	  :documentation
	  "A hash with as key and value the items that
make up the container")
   (excluded-items :type hash-table
		   :initform (make-hash-table :test #'eql)
		   :reader container-excluded-items
		   :documentation
	  "A hash with as key and value the items that
are exluded from the container"))
  (:documentation
   "The basic container contains objects but we also want to
know which objects it indirectly contains or has an
overlap with"))


(defmethod cl:print-object ((object container) stream)
  (print-unreadable-object (object stream)
    (format stream "container ~A" (item-name object))))
