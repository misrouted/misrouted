;;;  -*- Mode: Lisp -*-
;;; Copyright 2006, Peter Van Eynde
;;; License: LLGPL v2

(in-package :pvaneynd.matryoshka)

(defclass item ()
  ((name :type string
	 :initarg :name
	 :reader item-name
	 :documentation "The unique name of the item")
   (namespace :type namespace
	      :reader item-namespace
	      :documentation "The namespace in which the item exists")
   (id :type (integer 0)
       :reader item-id
       :documentation "The unique id of the item")
   (contained-in  :type hash-table
		  :reader item-contained-in
		  :documentation
		  "A hash table containing the containers that contain
the given item, with as key and value the containers"))
  (:documentation
   "The basic object that exists in a namespace.
An item can never contain another object nor overlap with another
object."))


(defmethod cl:print-object ((object item) stream)
  (print-unreadable-object (object stream)
    (format stream "item ~A" (item-name object))))


(defclass item-with-ip (item)
  ((ip :type (or nil pvaneynd.ip:ipaddress)
       :initform nil
       :reader item-ipaddress
       :documentation
       "The ip of the item")))
